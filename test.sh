#!/bin/sh -eu

bin=./target/release/chokudai_004
# bin=./target/debug/chokudai_004

cargo build --release

time $bin < in/example_01.txt > 01.txt
./a.out in/example_01.txt out/example_01.txt 01.txt

time $bin < in/example_02.txt > 02.txt
./a.out in/example_02.txt out/example_02.txt 02.txt

time $bin < in/example_03.txt > 03.txt
./a.out in/example_03.txt out/example_03.txt 03.txt
