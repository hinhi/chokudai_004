use std::io::{stdin, BufRead, stderr, Write};
use std::num::Wrapping as w;

struct Rng {
    x: w<u32>,
    y: w<u32>,
    z: w<u32>,
    w: w<u32>,
}

impl Rng {
    fn new() -> Rng {
        Rng {
            x: w(123456789),
            y: w(362436069),
            z: w(521288629),
            w: w(88675123),
        }
    }

    fn next_u32(&mut self) -> u32 {
        let x = self.x;
        let t = x ^ (x << 11);
        self.x = self.y;
        self.y = self.z;
        self.z = self.w;
        let w_ = self.w;
        self.w = w_ ^ (w_ >> 19) ^ (t ^ (t >> 8));
        self.w.0
    }

    fn gen(&mut self, x: u32) -> u32 {
        self.next_u32() % x
    }

    fn gen1(&mut self) -> f64 {
        self.next_u32() as f64 / 4294967296.0
    }
}

const N: usize = 30;

struct Board {
    board: [[i32; N]; N],
    best_board: [[i32; N]; N],
    bound: [[[i32; 2]; N]; N],
    count_row: [(i32, i32, i32); N],
    count_col: [(i32, i32, i32); N],
    b: (i32, i32, i32),
    score: i32,
    best_score: i32,
    bad_move: i32,
}

impl Board {
    fn new() -> Board{
        let stdin = stdin();
        let mut cin = stdin.lock();
        let (n, b) = {
            let mut line = String::new();
            cin.read_line(&mut line).unwrap();
            let mut words = line.split_whitespace();
            (
                words.next().unwrap().parse::<usize>().unwrap(),
                (
                    words.next().unwrap().parse::<i32>().unwrap(),
                    words.next().unwrap().parse::<i32>().unwrap(),
                    words.next().unwrap().parse::<i32>().unwrap(),
                )
            )
        };
        assert_eq!(n, N);

        let mut bound = [[[0, 0]; N]; N];
        let mut board = [[0; N]; N];
        for y in 0..N {
            let mut line = String::new();
            cin.read_line(&mut line).unwrap();
            for (x, word) in line.split_whitespace().enumerate() {
                bound[y][x][0] = word.parse().unwrap();
                board[y][x] = bound[y][x][0];
            }
        }
        for y in 0..N {
            let mut line = String::new();
            cin.read_line(&mut line).unwrap();
            for (x, word) in line.split_whitespace().enumerate() {
                bound[y][x][1] = word.parse().unwrap();
            }
        }

        let mut count_row = [(0, 0, 0); N];
        let mut count = (0, 0, 0);
        for (y, row)in board.iter().enumerate() {
            let c = count_b(row, b);
            count_row[y] = c;
            count.0 += c.0;
            count.1 += c.1;
            count.2 += c.2;
        }
        let mut count_col = [(0, 0, 0); N];
        for x in 0..N {
            let mut col = [0; N];
            for y in 0..N {
                col[y] = board[y][x];
            }
            let c = count_b(&col, b);
            count_col[x] = c;
            count.0 += c.0;
            count.1 += c.1;
            count.2 += c.2;
        }
        let score = count.0 * b.0 + count.1 * b.1 + count.2 * b.2;
        Board {
            board: board,
            best_board: board.clone(),
            bound: bound,
            score: score,
            count_row: count_row,
            count_col: count_col,
            b: b,
            best_score: score,
            bad_move: 0,
        }
    }

    fn print_ans(self) {
        for row in self.best_board.iter() {
            let mut line = String::new();
            for x in 0..N {
                if x == N - 1 {
                    line += &format!("{}", row[x]);
                } else {
                    line += &format!("{} ", row[x]);
                }
            }
            println!("{}", line);
        }
    }

    fn mc(&mut self, rng: &mut Rng, beta: f64) {
        let x = rng.gen(N as u32) as usize;
        let y = rng.gen(N as u32) as usize;
        let range = self.bound[y][x][1] - self.bound[y][x][0];
        if range == 0 {
            return;
        }
        let now = self.board[y][x];
        let r = 1 + rng.gen(range as u32) as i32;
        let new = self.bound[y][x][0] + (now + r - self.bound[y][x][0]) % (range + 1);
        debug_assert_ne!(new, now);
        debug_assert!(self.bound[y][x][0] <= new && new <= self.bound[y][x][1]);
        self.board[y][x] = new;

        let c_row = count_b(&self.board[y], self.b);
        let c_col = {
            let mut col = [0; N];
            for yy in 0..N {
                col[yy] = self.board[yy][x];
            }
            count_b(&col, self.b)
        };
        let diff = (c_row.0 - self.count_row[y].0 + c_col.0 - self.count_col[x].0) * self.b.0
            + (c_row.1 - self.count_row[y].1 + c_col.1 - self.count_col[x].1) * self.b.1
            + (c_row.2 - self.count_row[y].2 + c_col.2 - self.count_col[x].2) * self.b.2;
        let new_score = self.score + diff;
        if self.best_score < new_score {
            self.best_score = new_score;
            self.best_board = self.board.clone();
        }

        if diff > 0 || rng.gen1() < (beta * diff as f64).exp() {
            self.score = new_score;
            self.count_row[y] = c_row;
            self.count_col[x] = c_col;
            if diff < 0 {
                self.bad_move += 1;
            }
        } else {
            self.board[y][x] = now;
        }
    }
}

fn shakutori(xx: &[i32; N + 1], mut start: usize, mut end: usize, b: i32) -> i32 {
    let mut c = 0;
    while end <= N {
        let x = xx[end] - xx[start];
        if x < b {
            end += 1;
        } else if x == b {
            c += 1;
            start += 1;
        } else {
            start += 1;
        }
    }
    c
}

fn count_b(row: &[i32; N], b: (i32, i32, i32)) -> (i32, i32, i32) {
    let xx = {
        let mut xx = [0; N + 1];
        for (i, x) in row.iter().enumerate() {
            xx[i + 1] = xx[i] + *x;
        }
        xx
    };
    (
        shakutori(&xx, 0, 2, b.0),
        shakutori(&xx, 0, 3, b.1),
        shakutori(&xx, 0, 4, b.2),
    )
}

fn main() {
    let mut board = Board::new();
    let mut rng = Rng::new();
    writeln!(stderr(), "start {}", board.score).unwrap();
    let n = 2000000;
    for i in 0..n {
        let beta =  0.1 * (1.0 - (i as f64 / n as f64));
        board.mc(&mut rng, beta);
    }
    writeln!(stderr(), "end {}", board.best_score).unwrap();
    writeln!(stderr(), "bad {}", board.bad_move).unwrap();
    board.print_ans();
}
